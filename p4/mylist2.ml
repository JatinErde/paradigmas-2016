
(*List.hd*)
let hd = function
    h::_ -> h
  | [] -> raise (Failure "hd");;

(*val hd : 'a list -> 'a = <fun>*)

(*List.tl*)
let tl = function
    _::t -> t
    | [] -> raise (Failure "tl");;

(*val hd : 'a list -> 'a list = <fun>*)

(*List.length*)
let rec length l =
  let rec aux acc = function
    [] -> acc
    | h::t -> aux (acc + 1 ) t
  in aux 0 l ;;

(* val length : 'a list -> int = <fun> *)

(*List.nth *)

let nth l n =
      if (n< 0) then raise (Invalid_argument "nth")
      else
        let rec faux = function
            ([], _) -> raise (Failure "nth")
          | (h::_, 0) -> h
          |   (_::t, i) -> faux (t, i-1)
          in faux (l,n) ;;

(* val nth : 'a list -> int -> 'a = <fun> *)
(* List.append *)

let rec append l1 l2 = match l1 with
  [] -> l2
| h::t -> h::append t l2;;

(* val append : 'a list -> 'a list -> 'a list = <fun> *)

(*List.rev*)

let rev l =
  let rec aux acc = function
      [] -> acc
    | h::t -> aux (h::acc) t
    in aux [] l ;;

(* val rev : 'a list -> 'a list = <fun>*)

(* List.rev_append *)

(*let rev_append l1 l2 = append (rev l1) l2;;*)

let rec rev_append l1 l2 =
    match l1 with
    [] -> l2
    | h::t -> rev_append t (h::l2)

(* val rev_append : 'a list -> 'a list -> 'a list = <fun> *)

(*List.concat*)

let rec concat = function
    [] -> []
  |  h::t -> append h (concat t) ;;

(* val concat 'a list list -> 'a list = <fun> *)

(*List*)
let flatten = concat

(*List.map*)

let rec map f = function
    [] -> []
    | h::t -> f h::map f t ;;

(* val ('a -> 'b) -> 'a list -> 'b list = <fun> *)


(*List.map2 *)

  let rec map2 f l1 l2 = match l1, l2 with
      [], [] -> []
    | [], _ -> raise (Invalid_argument "map2")
    | _, [] -> raise (Invalid_argument "map2")
    | (h::t, h2::t2) -> (f h h2)::(map2 f t t2);;

(* val map2 : ('a -> 'b -> 'c) -> 'a list -> 'b list -> 'c list = <fun> *)

(*List.fold_left *)

let rec fold_left f a = function
    [] -> a
  | h::t -> fold_left f (f a h) t ;;


(* val fold_left : ('a -> 'b -> 'a) -> 'a -> 'b list -> 'a = <fun> *)

(*List.fold_right *)

let rec fold_right f l b =
    let rec aux la ba = match la with
        [] -> ba
       |h::t -> aux t (f h ba)
    in aux (List.rev l) b;;

(*val fold_right : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b = <fun> *)

(* List.find *)

let rec find p  = function
    [] -> raise (Not_found)
  | h::t ->  if p h then h
             else find p t ;;

(* val : ('a -> bool) -> 'a list -> 'a = <fun> *)

let rec for_all p  = function
    [] -> true
  | h::t ->  p h && for_all p t ;;

(* Es terminal porque el &&, || e if-else evaluan en cortocircuito *)

(* val for_all : ('a -> bool) -> 'a list -> bool = <fun> *)

(* List.exists *)

let rec exists p l =
    match l with
    | []  -> false
    | h::t -> if not (p h)
                then exists p t
                else true;;

(* val exists : ('a -> bool) -> 'a list -> bool = <fun> *)

(* List.mem *)

let rec mem a = function
  []-> false
  | h::t-> if a=h then true
              else mem a t;;


(* val mem : 'a -> 'b list -> bool = <fun> *)

(* List.filter *)

let filter p lista =
    let rec aux acc = function
        []-> rev acc
        |h::t -> if p h
                    then aux (h::acc) t
                    else aux acc t
    in aux[] lista;;

(* val : ('a -> bool) -> 'a list -> 'a list = <fun> *)

(* List.find_all *)

let find_all = filter ;;

(* val : : ('a -> bool) -> 'a list -> 'a list = <fun> *)

(* List.partition *)

let partition f lista =
    let rec aux l (l1, l2) = match l with
        [] -> (rev l1, rev l2)
        |h::t -> if f h then aux t ((h::l1), l2)
                else aux t (l1, (h::l2))
in aux lista ([], []);;

(* val partition : ('a -> bool) -> 'a list -> 'a list * 'a list = <fun> *)


(* List.split *)

let rec split = function
    [] ->  [],[]
  | (x,y)::t ->   let (xt, yt) = split t in (x::xt, y::yt) ;;

(*val split : ('a * 'b) list -> 'a list * 'b list = <fun> *)

(* List.combine *)

let rec combine = function
    []-> (function
            []-> []
            | g::u-> raise(Invalid_argument "combine"))
    | h1::t1-> (function
                []-> raise(Invalid_argument "combine")
                | h2::t2-> (h1,h2)::combine t1 t2);;


(* val  'a list -> 'b list -> ('a * 'b) list = <fun>*)

(* remove: 'a -> 'a list -> 'a list *)

let rec remove a l =
    let rec aux a l2 = function
        []-> rev(l2)
        | h::t-> if a=h then aux a (rev_append t l2) []
             else aux a (h::l2) t
    in aux a [] l;;

(* remove_all: 'a -> 'a list -> 'a list *)

let rec remove_all n l = match l with
    [] -> []
  | h::t -> if (n=h) then remove_all n t
            else  h::remove_all n t;;

(* ldif: 'a list -> 'a list -> 'a list *)

let rec ldif l1 = function
    [] -> l1
  | h::t -> ldif (remove_all h l1) t ;;


(* lprod: 'a list -> 'b list -> ('a * 'b) list *)

(*let lprod l1 l2 = flattern (map (function x -> map (function y -> (x,y) l2) l1 )))*)

  let lprod l1 l2 =
      let rl1 = rev l1 and rl2 = rev l2 in
      let rec aux = function
          [], _ ,l ->  l
        | _ ::t, [], l -> aux (t,rl2,l)
      	| h::t, r::s, l -> aux(h::t,s,(h,r)::l)
      in aux (rl1, rl2, []) ;;


(*  divide: 'a list -> 'a list * 'a list*)


let divide l =
  let rec aux l1 l2 = function
    [] -> List.rev l2, List.rev l1
    | [h] -> List.rev (h::l2), List.rev l1
    | h1::h2::t -> aux (h2::l1) (h1::l2) t
  in aux [] [] l;;
