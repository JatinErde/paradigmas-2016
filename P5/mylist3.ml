open List;;

(*val suml : int list -> int = <fun> *)

let suml l = List.fold_left (+) 0 l;;

(*
let suml l =
  let rec aux acc = function
    [] -> acc
    | h::t -> aux (acc + h) t
in aux 0 l ;;
*)

(* val maxl : 'a list -> 'a = <fun> *)

let maxl l =
  if l = [] then raise (Failure "maxl")
  else List.fold_left max (List.hd l) (List.tl l);;

(*
let maxl l =
  if l = [] then raise (Failure "maxl")
  else let rec aux m = function
          [] ->  m
          | hx::tx  -> if (m < hx) then (aux hx tx)
                       else aux m tx
                      in aux (hd l) (tl l) ;;
*)

(*fromto : val fromto : int -> int -> int list = <fun>*)

let fromto m n =
  let rec aux (i, l)=
    if   i < m then l
    else aux (i-1, i::l)
  in aux (n, []) ;;

(*to0from : val to0from : int -> int list = <fun> *)

let to0from n = List.rev (fromto 0 n);;

(*from1to : val to1from : int -> int list = <fun> *)

let from1to n = fromto 1 n;;

(* append : val append : 'a list -> 'a list -> 'a list = <fun> *)

let append a b =
    let rec aux acc = function
         [] -> acc
        | x::xs -> aux (x::acc) xs
    in aux b (List.rev a);;


let append l1 l2 =
      rev_append (rev l1 ) l2 ;;

(*concat : val concat : 'a list list -> 'a list = <fun> *)

let concat l =
    let rec aux acc = function
         [] -> List.rev acc
        | h::t -> aux (List.rev_append h acc) t
    in aux [] l ;;


let concat l  =
    List.rev  (List.fold_left (fun a b -> List.rev_append b a ) [] l );;


(* map : val map : ('a -> 'b) -> 'a list -> 'b list = <fun> *)

let map f l =
  let rec aux acc = function
    [] -> List.rev acc
    | h :: t -> aux (f h :: acc) t
  in aux [] l;;

(* power : val power : int -> int -> int = <fun> *)

let power x y =
  let rec aux acc = function
        0 -> acc
      | e -> aux  (x * acc )   (e - 1)
  in
    if y >= 0 then aux 1 y
    else invalid_arg "power";;


(* FALTA HACER fact *)

let fact n =
    let rec innerfact m o =
        if m = 0 then o
        else innerfact (m - 1) ((float m) *. o)
    in
    if n >= 0 then innerfact n 1.
    else invalid_arg "fact";;

(* fib : val fib : int -> int = <fun> *)


let fib  n =
  let rec aux i fi fant =
    if i=n then fi
    else aux (i+1) (fi + fant) fi
  in
    if n >= 0 then aux 0 0 1
    else invalid_arg "fib";;

(*

let fold_right f l b =
      fold_left (fun x y -> f b x) b (rev l);;
*)

let fold_right f l b =
  let rev_l = rev l
    in let rec fold_rr acc = function
    [] -> acc
    | hd::tl -> fold_rr (f hd acc) tl
 in fold_rr b rev_l;;
(*
let insecg l = List.fold_right (fun x t -> x::List.map ((+) x) t) l [];;

*)

(* inseg : val incsec : int list -> int list = <fun> *)

let incseg l =
  if l = [] then []
  else
    let rec aux acc = function
       [] -> rev acc
      | h::t ->  aux ((h+hd acc) :: acc ) t
  in aux [hd l] (tl l);;

(* val multicomp : ('a -> 'a) list -> 'a -> 'a = <fun>


*)

let multicomp l x =
   let rec aux acc l2 = match l2 with
      [] -> acc
      | f::t -> aux (f acc) t
   in aux x (List.rev(l));;


let insert x l =
  let add_x y cola =
    if x < y then x::y::(List.tl cola)
    else y::cola
  in fold_right add_x l [x]



let insert_gen f x l =
    let add_x y cola =
        if f x y then x::y::(List.tl cola)
        else y::cola
    in fold_right add_x l [x];;





(*hanoi : *)
(*

let hanoi n a b c  =
    0 -> []
  | (n-1) a c b @ ((a, []) :: (n-1) b a c ) ;;

  *)
