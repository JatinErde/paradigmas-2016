let lowercase  = function
  x -> if (x > 'A') && (x < 'Z') || (x > '\192') && (x < '\214') || (x > '\216') && (x < '\222')  then char_of_int(int_of_char(x)+32)
    else x ;;

let uppercase  = function
    x -> if (x > 'a') && (x < 'z') || (x > '\224') && (x <'\246') || (x> '\248') && (x <'\254') then char_of_int(int_of_char(x)-32)
      else x ;;
