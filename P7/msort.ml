
(*
let rec rlist r n =
  if n < 1 then []
  else Random.int r :: rlist r (n-1) ;;
  *)



(* 1. Implemente (con el mismo tipo) una versión r_list_t recursiva terminal. *)

(* rlist : int -> int -> int list = <fun> *)

let rlist r n =
  let rec aux acc n  =  if n < 1 then acc
                        else aux (Random.int r::acc) (n-1)

  in aux [] n ;;


(* 2. Defina, utilizando las funciones divide y merge, una función m_sort  *)

(*
  let rec divide = function
    h1::h2::t -> let t1, t2 = divide t in h1::t1, h2::t2
    | l -> l, [] ;;


  let rec merge ord (l1, l2) = match l1, l2 with
      [], l | l, [] -> l
    | h1::t1,h2::t2 ->  if ord h1 h2
                        then h1::merge ord (t1, l2)
                        else h2::merge ord (l1, t2) ;;

*)

    let rec m_sort f l =
        let n = List.length l in
          if n > 1 then
              let (l1, l2) = divide l
              in merge f ( m_sort f l1, m_sort f l2)
          else  l;;



(* 3. Defina de modo recursivo terminal funciones divide_t y merge_t (con el mismo tipo que divide
y merge, respectivamente) que sirvan para el mismo objetivo. *)





(* divide_t : 'a list -> 'a list * 'a list = <fun> *)

  let divide_t l =
      let rec aux (l1,l2) = function
          [] -> l1,l2
        | h::[] -> h::l1, l2
        | h1::h2::t -> aux (h1::l1,h2::l2) t
      in aux ([],[]), l;;



(* merge_t : ('a -> 'a -> bool) -> 'a list * 'a list -> 'a list = <fun> *)


let merge_t ord (l1,l2) =
    let rec aux res = function
        [],[] -> List.rev res
      | [],l | l, [] -> List.rev_append res l
      | h1::t1, h2::t2 -> if ord h1 h2
                          then aux (h1::res) (t1, h2::t2)
                          else aux (h2::res) (h1::t1, t2)
    in aux [] (l1,l2);;






(* 4. Realice una nueva implementación del método de ordenación por fusión, m_sort2:
('a -> 'a -> bool) -> 'a list -> 'a list, utilizando ahora las funciones divide_t y merge_t*)


let rec m_sort f l =
    let n = List.length l in
      if n > 1 then
          let (l1, l2) = divide_t l
          in merge_t f ( m_sort f l1, m_sort f l2)
      else  l;;





(* 5.*)
