
type ari_exp =
  Const of int
  | Sum of ari_exp * ari_exp
  | Pro of ari_exp * ari_exp
  | Dif of ari_exp * ari_exp
  | Quo of ari_exp * ari_exp
  | Mod of ari_exp * ari_exp ;;


(* Ejemplo:  2 * 3 +1 ->  Sum(Pro(Const 2, Const 3), Const 1);; *)

let rec eval  = function
   Const n -> n
  | Sum(e1, e2) -> eval e1 + eval e2
  | Pro(e1, e2) -> eval e1 * eval e2
  | Dif(e1, e2) -> eval e1 - eval e2
  | Quo (e1, e2) -> eval e1 / eval e2
  | Mod (e1, e2) -> (eval e1) mod (eval e2);;


type biOper = Sm
            | Pr
            | Df
            | Qt
            | Md ;;



type exp = C of int
          | Op of biOper * exp * exp;;

(* Ejemplo: 2 * 3 +1 ->  Op(Sm, Op (Pr , C 2, C 3), C 1)*)


(* a)  exp_of_ari_exp : ari_exp -> exp  *)

let rec exp_of_ari_exp  = function
    Const n -> C n
    | Sum(e1, e2) -> Op (Sm, exp_of_ari_exp e1, exp_of_ari_exp e2)
    | Pro(e1, e2) -> Op (Pr, exp_of_ari_exp e1, exp_of_ari_exp e2)
    | Dif(e1, e2) -> Op (Df, exp_of_ari_exp e1, exp_of_ari_exp e2)
    | Quo (e1, e2) -> Op (Qt, exp_of_ari_exp e1, exp_of_ari_exp e2)
    | Mod (e1, e2) -> Op (Md, exp_of_ari_exp e1, exp_of_ari_exp e2) ;;



(* ari_exp_of_exp: exp -> ari_exp *)

let rec ari_exp_of_exp = function
    C n -> Const n
  | Op (Sm, e1, e2) -> Sum( ari_exp_of_exp  e1, ari_exp_of_exp e2)
  | Op (Pr, e1,  e2) -> Pro( ari_exp_of_exp e1, ari_exp_of_exp e2)
  | Op (Df, e1, e2) -> Dif( ari_exp_of_exp e1, ari_exp_of_exp e2)
  | Op (Qt, e1, e2) -> Quo(ari_exp_of_exp e1, ari_exp_of_exp e2)
  | Op (Md, e1, e2) ->  Mod(ari_exp_of_exp e1, ari_exp_of_exp e2) ;;






(* b) opval: biOper -> (int -> int -> int) *)

let opval = function
   Sm -> (+)
  | Pr -> ( * )
  | Df -> (-)
  | Qt -> (/)
  | Md -> (mod) ;;

(* eval : exp -> int *)
  let rec eval = function
    C n  -> n
  | Op (o, e1, e2) -> opval o (eval e1) (eval e2) ;;


(* c) *)

(*
let conmut = function 
    Const n ­> Const n
  | Sum (e1,e2) ­-> Sum (e2,e1)
  | Pro (e1,e2) ­-> Pro (e2,e1)
  | Dif (e1,e2) ­-> Dif (e2,e1)
  | Quo (e1,e2) ­-> Quo (e2,e1)
  | Mod (e1,e2) ­-> Mod (e2,e1);; *)


let conmut = function
     C n -> C n
    | Op (o, e1, e2) -> Op (o, e2, e1);;


(*

let rec mirror = function 
    Const n ­> Const n
  | Sum (e1,e2) ­-> Sum (mirror e2, mirror e1)
  | Pro (e1,e2) ­-> Pro (mirror e2, mirror e1)
  | Dif (e1,e2) ­-> Dif (mirror e2, mirror e1)
  | Quo (e1,e2) ­-> Quo (mirror e2, mirror e1)
  | Mod (e1,e2) ­-> Mod (mirror e2, mirror e1);;

*)


  let rec mirror = function
      C n -> C n
    | Op (o, e1, e2) ->  Op( o, mirror e2, mirror e1);;



(*
let rec shift_left = function
    Sum (x, Sum (y,z)) -> shift_left (Sum (Sum (x,y), z))
    |Pro (x, Pro (y,z)) -> shift_left (Pro (Pro (x,y), z))
    |Sum (e1, e2) -> Sum (shift_left e1, shift_left e2)
    |Pro (e1, e2) -> Pro (shift_left e1, shift_left e2)
    |Dif (e1, e2) -> Dif (shift_left e1, shift_left e2)
    |Quo (e1, e2) -> Quo (shift_left e1, shift_left e2)
    |Mod (e1, e2) -> Mod (shift_left e1, shift_left e2)
    | e-> e;;

*)


let rec shift_left = function
      C n -> C n
    | Op (Sm, x, Op(Sm, y,z)) ->  shift_left (Op (Sm, Op (Sm, x, y), z))
    | Op (Pr, x, Op(Pr, y, z)) ->  shift_left(Op(Pr, Op (Pr, x, y), z))
    | Op (o, e1, e2) ->  Op( o, shift_left e1, shift_left e2);;

(*
    let rec str_of_exp = function 
        Const n ­-> string_of_int n
      | Sum (x,y) -­> "(" ^ str_of_exp x ^ " + " ^ str_of_exp y ^ ")"
      | Pro (x,y) -­> "(" ^ str_of_exp x ^ " * " ^ str_of_exp y ^ ")"
      | Dif (x,y) -­> "(" ^ str_of_exp x ^ " ­ " ^ str_of_exp y ^ ")"
      | Quo (x,y) -­> "(" ^ str_of_exp x ^ " / " ^ str_of_exp y ^ ")"
      | Mod (x,y) ­-> "(" ^ str_of_exp x ^ " % " ^ str_of_exp y ^ ")";;
*)

let str_of_op = function
   Sm -> " + "
  | Pr -> " * "
  | Df -> " - "
  | Qt -> " / "
  | Md -> " % " ;;


let rec str_of_exp = function
      C n -> string_of_int n
      | Op (o, x, y) -> "(" ^ str_of_exp x ^ str_of_op o ^ str_of_exp y ^ ")" ;;


(*
  let rec rpn = function
            Const n -> " " ^ string_of_int n
            | Sum (e1, e2) -> rpn e1 ^rpn e2 ^" + "
            | Pro (e1, e2) -> rpn e1 ^rpn e2 ^" * "
            | Dif (e1, e2) -> rpn e1 ^rpn e2 ^" - "
            | Quo (e1, e2) -> rpn e1 ^rpn e2 ^" / "
            | Mod (e1, e2) -> rpn e1 ^rpn e2 ^" % ";;

*)


let rec rpn = function
            C n -> " " ^ string_of_int n
          | Op (o, x, y) -> rpn x ^ rpn y ^ str_of_op o ;;


(* (d.) Ponga ejemplos (ilustrativos, pero lo más sencillos que pueda) de valores e: exp tales que *)


let e1 = Op (Sm, C 1, C 1);;
let e2 = Op (Df, C 2, C 4);;
let e3 = Op(Sm, Op (Pr , C 2, C 3), C 1);;
let e4 = Op (Sm, C 3, C 1);;
let e5 = Op(Sm, C 1, Op(Sm, C 2, C 3));;

(* conmut e1 = e1

= - : exp = Op (Sm, C 1, C 1) *)


(*  conmut e2 <> e2

conmut e2 - : exp = Op (Df, C 4, C 2)
e2 - : exp = Op (Df, C 2, C 4)

*)


(*  conmut e1 = mirror e1

conmut e1 = - : exp = Op (Sm, C 1, C 1)
mirror e1 = - : exp = Op (Sm, C 1, C 1)

*)


(*  conmut e <> mirror e

  conmut e3 <> mirror e3∫


*)


(* eval e1 = eval (conmut e1)

eval e1 =  - : int = 2
eval (conmut e1) - : int = 2

*)


(*  eval e4 <> eval (conmut e4)

eval e4 = - : int = 4

eval (conmut e4) = - : int = 4

*)


(*  eval e4 <> eval (mirror e4)

eval e4 = - : int = 4

eval (mirror e4) = - : int = 4

*)


(* eval e = eval (conmut e) && eval e <> eval (mirror e)


*)


(* eval e5 = eval (shift_left e5)

eval e5 = - : int = 6
eval (shift_left e5) = : - int = 6

*)


(* eval e <> eval (shift_left e)

No se puede ya que la shift_left(asoactiva) solo la estamos aplicando al producto y la suma

*)
