(* Una expresión de tipo int que contenga, al menos, 4 operadores infijos.*)
(1 + 2 - 2) * 5 / 2;;

(*Una expresión de tipo float que incluya una función predefinida. *)
abs_float (-1.);;

(*Una expresión de tipo char que incluya una sub-expresión de tipo int.*)
Char.chr(60+5);;

(* Una expresión no trivial de tipo bool*)
not (2=2);;

(* Una expresión de tipo string que contenga una estructura if-then-else. *)

if 2 > 1 then "caml funciona bien" else "caml funciona mal";;

(* Una expresión no trivial de tipo int * int. *)

max (2,3) (1,4);;
