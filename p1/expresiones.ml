();;
(* - : unit = () *)

2+5 * 3;;
(* - : int = 17*)

1.0;;
(* - : float = 1 *)

(*1.0 * 2;;*)
(*Error de tipo, ya que 1.0 es un float y '*' es para multiplicar enteros*)

1.0 *. 2.0;;
(* - : float = 2. *)

(*2 - 2.0;;*)
(*Error de tipo, ya que intenta restar un entero a un float*)

2.0 -. 2.0;;
(* - : float = 0. *)

(*3.0 + 2.0;;*)
(*Error de tipo ya que habria que poner la funcion + en float*)

3.0 +. 2.0;;
(* - : float =  5. *)

5 / 3;;
(*:- int  = 1*)

5 mod 3;;
(* - : int = 2 *)

3.0 *. 2.0 ** 3.0;;
(* - : float = 24. *)

3.0 = float_of_int 3;;
(* - : bool = true *)

(* sqrt 4;; *)
(* error de tipo, 4 debe ser float, es decir, '4.' *)

sqrt 4.;;
(* - : float = 2 *)

(* int_of_float 2.1 + int_of_float (-2,9);; *)
(* Error de tipo, ya que 2,9 no es un float si no de tipo int * int *)

int_of_float 2.1 + int_of_float (-2.9);;
(* - : int = 0  *)

truncate 2.1 + truncate (-2.9);;
(* - : int = 0 *)

floor 2.1 +. floor (-2.9);;
(* - : float = -1 *)

(*ceil 2.1 +. ceil -2.9;;*)
(* Error de tipo lexico, faltan poner los parentesis en (-2,9). Si no los pones, '-' es la resta entera y ceil no es un int  *)

ceil 2.1 +. ceil (-2.9);;
(* - : float = 1 *)

'B';;
(* - : char = 'B' *)

int_of_char 'A';;
(* - : int = '65' *)

char_of_int 66;;
(* - : char = 'B' *)

Char.code 'B';;
(* - : int = 66 *)

Char.chr 67;;
(* - : char = 'C' *)

'\067';;
(* - : char = 'C' *)

Char.chr (Char.code 'a' - Char.code 'A' + Char.code '�');;
(* - : char = '\241' *)

Char.uppercase '�';;
(* - : char = '\209' *)

Char.lowercase 'O';;
(* - : char = 'o' *)

"this is a string";;
(* - : strin = "this is a string" *)

String.length "longitud";;
(* - : int = 8 *)

(*"1999" + "1";;*)
(* error de tipo: no se pueden sumar dos strings *)

"1999" ^ "1";;
(* - : string = "19991" *)

int_of_string "1999" + 1;;
(*- : int = 2000*)

"\064\065";;
(* - : string = @A*)

(*int_of_string 010;;*)
(* error de tipo, no es un string *)

int_of_string "010";;
(*: - int : 010 *)

not true;;
(* - : bool = false *)

true && false;;
(* - : bool = false *)

true || false;;
(* - : bool = true *)

(1 < 2) = false;;
(* - : bool = false *)

"1" < "2";;
(* - : bool = true *)

"2 "  < "12";;
(* - : bool = false *)

"uno" < "dos";;
(* - : bool = false *)

2,5;;
(* - : int * int = (2,5) *)

"hola", "adios";;
(* - : string * string = ("hola", "adios") *)

0, 0.0;;
(* - : int * float = (0, 0.)*)

fst('a',0);;
(* - : char = 'a' *)

snd (false, true);;
(* - : bool = true *)

(1,2,3);;
(* - : int * int * int = (1, 2, 3) *)

(1,2),3;;
(* - : (int * int) * int = ((1,2),3) *)

fst ((1,2),3);;
(* - : int * int = (1,2) *)


if 3 = 4 then 0 else 4;;
(* - : int = 4 *)

if 3 = 4 then "0" else "4";;
(* - : string = "4" *)

(*if 3 = 4 then 0 else "4";;*)
(* Error: This expression has type string but an expression was expected of type int *)

(if 3 < 5 then 8 else 10) + 4;;
(*- : int = 12 *)

let pi = 2.0 *. asin 1.0;;
(* val pi : float = 3.14159265358979312 *)

sin (pi/. 2.);;
(*- : float = 1.*)
